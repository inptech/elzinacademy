
CREATE DATABASE ElzinDatabaseExample

CREATE SCHEMA ElzinSchemaExample

CREATE TABLE ElzinTableExampleSQL3 (
FirstName VARCHAR (30),
LastName VARCHAR (30),
Age int,
Gender VARCHAR(1),
Place VARCHAR(25)
)

SELECT FirstName, LastName, Age, Gender,  FROM ElzinTableExampleSQL3

INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Ervin', 'Bajric', 19, 'M','Doboj')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Amela', 'Agic', 19, 'F','Zenica')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Elzin', 'Kalabic', 18, 'M','Srajevo')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Anastasija', 'Pobric', 18, 'F','Mostar')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Kenan', 'Huseinbasic', 18, 'M','Livno')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Eldar', 'Nezic', 20, 'M','Tesanj')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Ivan', 'Lazic', 22, 'M','Bihac')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Marko', 'Filipovic', 18, 'M','Travnik')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('David', 'Kantic', 30, 'M','Jajce')
INSERT INTO ElzinTableExampleSQL3 (FirstName, LastName, Age, Gender, Place) VALUES ('Luka', 'Bojovic', 32, 'M','Banja Luka')

ALTER TABLE ElzinTableExampleSQL3
    ADD Place VARCHAR(30)

	SELECT * FROM ElzinTableExampleSQL3

DELETE FROM ElzinTableExampleSQL